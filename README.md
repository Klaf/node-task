# Hello world

My first markdown

## Here is second level header

Ordered list:

1. First
2. Second

## Terminal commands

Code block: 

```sh
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum.toString());
})
```


<h2>Task list</h2>

- [x] Does the development branch exist
- [x] prevent node_modules from ending up into the gitlab.com
- [x] one default branch
- [x] documented the JavaScript function correctly with JSDoc
- [x] any effort in the markdown
- [ ] `Markdown HTML - Extra assignment`

## Some table

| File | Link |
| ------ | ------ |
| .gitignore | [/blob/main/.gitignore][PlDb] |
| README.md | [/blob/main/README.md][PlGh] |
| example_v1.5.png | [/blob/main/example_v1.5.png][PlGd] |
| main.js | [/blob/main/main.js][PlOd] |
| package-lock.json | [/blob/main/package-lock.json][PlMe] |
| package.json | [/blob/main/package.json][PlGa] |
| rest.http | [blob/main/rest.http][PlGb] |

![some_picture_here](./example_v1.5.png)

   [PlDb]: <https://gitlab.com/Klaf/node-task/-/blob/main/.gitignore>
   [PlGh]: <https://gitlab.com/Klaf/node-task/-/blob/main/README.md>
   [PlGd]: <https://gitlab.com/Klaf/node-task/-/blob/main/example_v1.5.png>
   [PlOd]: <https://gitlab.com/Klaf/node-task/-/blob/main/main.js>
   [PlMe]: <https://gitlab.com/Klaf/node-task/-/blob/main/package-lock.json>
   [PlGa]: <https://gitlab.com/Klaf/node-task/-/blob/main/package.json>
   [PlGb]: <https://gitlab.com/Klaf/node-task/-/blob/main/rest.http>